package id.co.telkomsigma.cache.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import id.co.telkomsigma.cache.model.Product;

public interface ProductDao extends PagingAndSortingRepository<Product, String>{
	
	public Product findByName(String name);
	
	public Product findByCode(String code);
    
    @Query("SELECT p FROM Product p WHERE p.name=:name")
    public List<Product> findDataByname (@Param("name") String name);

}
