package id.co.telkomsigma.cache.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.telkomsigma.cache.model.Product;
import id.co.telkomsigma.cache.service.ProductService;

@RestController
@RequestMapping("/product")
public class ProductController {
	
	@Autowired
	private ProductService productService;
	
	private static Logger logger = LoggerFactory.getLogger(ProductController.class);

	@GetMapping("/get/name/{name}")
	public Product findProductByName(@PathVariable(name="name") String name) {
		logger.info("Controller Find Product By Name");
		return productService.findByName(name);
	}

	@GetMapping("/get/code/{code}")
	public Product findProductByCode(@PathVariable(name="code") String code) {
		logger.info("Controller Find Product By Code");
		return productService.findByCode(code);
	}
	
	@GetMapping("/set/name/{id}/{name}")
	public Product updateProductWithName(@PathVariable(name="id") String id, @PathVariable(name="name") String name) {
		logger.info("Controller Update Product With Name");
		return productService.updateProductWithName(id, name);
	}
	
	@GetMapping("/delete/code/{code}")
	public String updateProductWithName(@PathVariable(name="code") String code) {
		logger.info("Controller Update Product With Name");
		return productService.deleteProductByCode(code).getName() + " Deleted";
	}
	
}
