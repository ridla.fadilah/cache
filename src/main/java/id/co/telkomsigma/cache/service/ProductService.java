package id.co.telkomsigma.cache.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.cache.dao.ProductDao;
import id.co.telkomsigma.cache.model.Product;

@Service
@CacheConfig(cacheNames="products")
public class ProductService {
	
	@Autowired
	private ProductDao productDao;
	
	private static Logger logger = LoggerFactory.getLogger(ProductService.class);

    @Cacheable(key = "#name")
    public List<Product> findProductListByName(String name) {
        logger.info("Service Find Product List By Name");
        return productDao.findDataByname(name);
    }
    
	@Cacheable(key="#name") // Enable CacheConfig
	//@Cacheable(value ="products", key="#name")
	public Product findByName(String name) {
		logger.info("Service Find Product By Name");
		return productDao.findByName(name);
	}

	@Cacheable(key="#code") // Enable CacheConfig
	//@Cacheable(value={ "products" }, key="#code")
	public Product findByCode(String code) {
		logger.info("Service Find Product By Code");
		return productDao.findByCode(code);
	}

	@CachePut(key="#name") // Enable CacheConfig
	//@CachePut(value="products", key="#name")
	public Product updateProductWithName(String id, String name) {
		logger.info("Service Update Product With Name");
		Product product = productDao.findOne(id);
		product.setName(name);
		try {
			productDao.save(product);
		} catch (Exception e) {
			logger.error("Service Update Product With Name - Error : ", e);
		}
		return product;
	}

	@CacheEvict(key="#code") // Enable CacheConfig
	//@CacheEvict(value="products", key="#code")
	public Product deleteProductByCode(String code) {
		logger.info("Service Delete Product By Code");
		Product product = productDao.findByCode(code);
		try {
			//productDao.delete(product);
		} catch (Exception e) {
			logger.error("Service Delete Product By Code - Error : ", e);
		}
		return product;
	}

}
