package id.co.telkomsigma.cache.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Data
@Entity
@Table
public class Product implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6764597235883377554L;
	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid2")
	private String id;
	private String name;
	private String code;
	private BigDecimal amount;
	private Boolean outOfStock;
	
}
