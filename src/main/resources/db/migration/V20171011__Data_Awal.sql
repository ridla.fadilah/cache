insert into product (id, name, code, amount, out_of_stock) VALUES
('p001', 'iPhone X', 'P-001', 15000000, true);

insert into product (id, name, code, amount, out_of_stock) VALUES
('p002', 'Macbook', 'P-002', 20000000, true);

insert into product (id, name, code, amount, out_of_stock) VALUES
('p003', 'iWatch', 'P-003', 5000000, true);

insert into product (id, name, code, amount, out_of_stock) VALUES
('p004', 'iPod', 'P-004', 4000000, true);

insert into product (id, name, code, amount, out_of_stock) VALUES
('p005', 'Mighty Mouse Apple', 'P-005', 500000, false);
